package http_server2

import "math"

func WithHost(host string) FnOption {
	return func(h *HttpServer) {
		h.host = host
	}
}

func WithPort(min, max int) FnOption {
	return func(h *HttpServer) {
		h.minPort = min
		h.maxPort = int(math.Max(float64(min), float64(max)))
	}
}

func WithAuth(handle FnAuthHandle) FnOption {
	return func(h *HttpServer) {
		h.auth = handle
	}
}

func WithPublic(path, method string, model Request, handle FnPublicHandle) FnOption {
	return func(h *HttpServer) {
		h.pubicHandle = append(h.pubicHandle, &publicHandle{
			path:   path,
			method: method,
			fn:     handle,
			model:  model,
		})
	}
}

func WithPrivate(path, method string, model Request, handle FnPrivateHandle) FnOption {
	return func(h *HttpServer) {
		h.privateHandle = append(h.privateHandle, &privateHandle{
			path:   path,
			method: method,
			fn:     handle,
			model:  model,
		})
	}
}

func WithUpload(path string, handle FnUploadHandle) FnOption {
	return func(h *HttpServer) {
		h.uploadHandle = append(h.uploadHandle, &uploadHandle{
			path: path,
			fn:   handle,
		})
	}
}

func WithUploadLimit(kb int64) FnOption {
	return func(h *HttpServer) {
		h.uploadMaxKB = kb
	}
}

func WithHostHeader(header string) FnOption {
	return func(h *HttpServer) {
		h.remoteHostHeader = header
	}
}
