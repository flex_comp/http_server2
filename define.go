package http_server2

import (
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"google.golang.org/protobuf/proto"
	"net/http"
)

type Request interface {
	proto.Message
}

type Response interface {
	proto.Message
}

type UploadResponse interface {
	GetFileKey() string
	GetHost() string
	GetHostDir() string
	GetDir() string
	GetFileName() string
}

type FnAuthHandle func(*http.Request) (interface{}, error)
type FnPublicHandle func(Request, string) (Response, error)
type FnPrivateHandle func(interface{}, Request, string) (Response, error)
type FnUploadHandle func(interface{}) (UploadResponse, error)

type FnOption func(*HttpServer)

type publicHandle struct {
	method string
	path   string
	fn     FnPublicHandle
	model  Request
}

type privateHandle struct {
	method string
	path   string
	fn     FnPrivateHandle
	model  Request
}

type uploadHandle struct {
	path string
	fn   FnUploadHandle
}

const (
	GET    = "GET"
	POST   = "POST"
	PATCH  = "PATCH"
	PUT    = "PUT"
	DELETE = "DELETE"
)

const (
	CodeAuthFailed     = 10000
	DecodeParamsFailed = 10001
	GetFileFailed      = 10002
	OpenFileFailed     = 10003
	CreateDirFailed    = 10004
	CreateFileFailed   = 10005
	CopyFileFailed     = 10006
	GetBodyFailed      = 10007
	BodyMustBeJson     = 10008
	MergeParamsFailed  = 10009
	InternalError      = 10010
)

func newResponseBodyFromMap(data map[string]interface{}) []byte {
	b, err := util.JSONMarshal(data)
	if err != nil {
		log.Error("响应Body失败:", err)
		return nil
	}

	return b
}

func newResponseBody(data Response) []byte {
	b, err := util.JSONMarshal(data)
	if err != nil {
		log.Error("响应Body失败:", err)
		return nil
	}

	return b
}

func newCodeBody(code int64) []byte {
	b, err := util.JSONMarshal(map[string]interface{}{
		"code": code,
	})

	if err != nil {
		log.Error("响应Body失败:", err)
		return nil
	}

	return b
}
