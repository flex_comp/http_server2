package http_server2

import (
	"errors"
	"fmt"
	"github.com/kataras/iris/v12"
	"github.com/kataras/iris/v12/context"
	"github.com/kataras/iris/v12/core/router"
	"gitlab.com/flex_comp/comp"
	"gitlab.com/flex_comp/log"
	"gitlab.com/flex_comp/util"
	"io"
	"net/http"
	"os"
	path2 "path"
	"reflect"
	"time"
)

var (
	ins *HttpServer
)

func init() {
	ins = &HttpServer{
		minPort: 9000,
		maxPort: 9100,
		auth: func(request *http.Request) (interface{}, error) {
			return nil, errors.New("need auth function")
		},
		uploadMaxKB: 8192,
	}
	_ = comp.RegComp(ins)
}

type HttpServer struct {
	host             string
	minPort          int
	maxPort          int
	auth             FnAuthHandle
	pubicHandle      []*publicHandle
	privateHandle    []*privateHandle
	uploadHandle     []*uploadHandle
	uploadMaxKB      int64
	remoteHostHeader string

	done chan bool
}

func (h *HttpServer) Init(map[string]interface{}, ...interface{}) error {
	return nil
}

func (h *HttpServer) Start(...interface{}) error {
	go h.run()
	return nil
}

func (h *HttpServer) UnInit() {

}

func (h *HttpServer) Name() string {
	return "http-server2"
}

func Option(opts ...FnOption) {
	ins.Option(opts...)
}

func (h *HttpServer) Option(opts ...FnOption) {
	for _, opt := range opts {
		opt(h)
	}
}

func (h *HttpServer) run() {
	i := iris.New()
	h.handlePublic(i.Party("/public"))
	h.handlePrivate(i.Party("/private", h.doAuth))
	h.handleUpload(i.Party("/upload", h.doAuth))

	port := h.minPort

	var cfg []iris.Configurator
	if len(h.remoteHostHeader) > 0 {
		cfg = append(cfg, iris.WithRemoteAddrHeader(h.remoteHostHeader))
	}

	for {
		select {
		case <-h.done:
		default:
			addr := fmt.Sprintf("%s:%d", h.host, port)
			log.Warn("尝试监听:", addr)
			err := i.Listen(addr, cfg...)
			if err != nil {
				port++
				if port > h.maxPort {
					port = h.minPort
				}

				log.Warn("监听失败, 1秒后尝试新地址:", addr)
				<-time.After(time.Second)
			}
		}
	}
}

func (h *HttpServer) doAuth(ctx context.Context) {
	id, err := h.auth(ctx.Request())
	if err != nil {
		_, _ = ctx.Write(newCodeBody(CodeAuthFailed))
		return
	}

	ctx.Values().Set("id", id)
	ctx.Next()
}

func (h *HttpServer) method(p router.Party, m string) func(string, ...context.Handler) *router.Route {
	switch m {
	case GET:
		return p.Get
	case POST:
		return p.Post
	case PUT:
		return p.Put
	case DELETE:
		return p.Delete
	case PATCH:
		return p.Patch
	}

	return nil
}

func (h *HttpServer) handlePublic(public router.Party) {
	for _, handle := range h.pubicHandle {
		hd := handle
		fn := h.method(public, hd.method)
		if fn == nil {
			log.Warn("未知方法:", hd.method)
			continue
		}

		fn(hd.path, func(ctx context.Context) {
			var (
				res Response
				err error
			)

			if hd.model == nil {
				res, err = hd.fn(nil, ctx.RemoteAddr())
			} else {
				var merged []byte
				merged, err = h.mergeParams(ctx)
				if err != nil {
					return
				}

				val := reflect.New(reflect.TypeOf(hd.model))
				err = util.JSONUnmarshal(merged, val.Interface())
				if err != nil {
					_, _ = ctx.Write(newCodeBody(DecodeParamsFailed))
					return
				}

				res, err = hd.fn(val.Elem().Interface().(Request), ctx.RemoteAddr())
			}

			if err != nil {
				_, _ = ctx.Write(newCodeBody(InternalError))
				return
			}

			_, _ = ctx.Write(newResponseBody(res))
			ctx.Next()
		})
	}
}

func (h *HttpServer) handlePrivate(public router.Party) {
	for _, handle := range h.privateHandle {
		hd := handle
		fn := h.method(public, hd.method)
		if fn == nil {
			log.Warn("未知方法:", hd.method)
			continue
		}

		fn(hd.path, func(ctx context.Context) {
			var (
				res Response
				err error
			)

			if hd.model == nil {
				res, err = hd.fn(ctx.Values().Get("id"), nil, ctx.RemoteAddr())
			} else {
				var merged []byte
				merged, err = h.mergeParams(ctx)
				if err != nil {
					return
				}

				val := reflect.New(reflect.TypeOf(hd.model))
				err = util.JSONUnmarshal(merged, val.Interface())
				if err != nil {
					_, _ = ctx.Write(newCodeBody(DecodeParamsFailed))
					return
				}

				res, err = hd.fn(ctx.Values().Get("id"), val.Elem().Interface().(Request), ctx.RemoteAddr())
			}

			if err != nil {
				_, _ = ctx.Write(newCodeBody(InternalError))
				return
			}

			_, _ = ctx.Write(newResponseBody(res))
			ctx.Next()
		})
	}
}

func (h *HttpServer) handleUpload(upload router.Party) {
	for _, handle := range h.uploadHandle {
		hd := handle
		upload.Post(hd.path, iris.LimitRequestBodySize(h.uploadMaxKB*1024), func(ctx context.Context) {
			res, err := hd.fn(ctx.Values().Get("id"))
			if err != nil {
				_, _ = ctx.Write(newCodeBody(InternalError))
				return
			}

			_, fh, err := ctx.FormFile(res.GetFileKey())
			if err != nil {
				_, _ = ctx.Write(newCodeBody(GetFileFailed))
				return
			}

			src, err := fh.Open()
			if err != nil {
				_, _ = ctx.Write(newCodeBody(OpenFileFailed))
				return
			}

			defer src.Close()

			err = os.MkdirAll(path2.Join(res.GetHostDir(), res.GetDir()), os.ModePerm)
			if err != nil {
				_, _ = ctx.Write(newCodeBody(CreateDirFailed))
				return
			}

			out, err := os.Create(path2.Join(res.GetHostDir(), res.GetDir(), res.GetFileName()))
			if err != nil {
				_, _ = ctx.Write(newCodeBody(CreateFileFailed))
				return
			}

			defer out.Close()

			_, err = io.Copy(out, src)
			if err != nil {
				_, _ = ctx.Write(newCodeBody(CopyFileFailed))
				return
			}

			_, _ = ctx.Write(newResponseBodyFromMap(map[string]interface{}{
				"code": 1,
				"url":  path2.Join(res.GetHost(), res.GetDir(), res.GetFileName()),
			}))
			ctx.Next()
		})
	}
}

func (h *HttpServer) mergeParams(ctx context.Context) ([]byte, error) {
	m := make(map[string]interface{})
	urls := ctx.URLParams()

	body, err := ctx.GetBody()
	if err != nil {
		_, _ = ctx.Write(newCodeBody(GetBodyFailed))
		return nil, err
	}

	if len(body) > 0 {
		err = util.JSONUnmarshal(body, &m)
		if err != nil {
			_, _ = ctx.Write(newCodeBody(BodyMustBeJson))
			return nil, err
		}
	}

	for k, v := range urls {
		m[k] = v
	}

	merged, err := util.JSONMarshal(m)
	if err != nil {
		_, _ = ctx.Write(newCodeBody(MergeParamsFailed))
		return nil, err
	}

	return merged, nil
}
