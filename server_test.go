package http_server2

import (
	"fmt"
	"gitlab.com/flex_comp/comp"
	uid "gitlab.com/flex_comp/uid2"
	"gitlab.com/flex_comp/util"
	"gitlab.com/msex/model_pb/kitex_gen/model/m_user"
	"net/http"
	"testing"
	"time"
)

func TestHttpServer_run(t *testing.T) {
	t.Run("all", func(t *testing.T) {
		_ = comp.Init(map[string]interface{}{
			"log.path": "./test/srv.log",
			"log.size": 50,
			"log.age":  1,
		})
		close := make(chan bool, 1)

		ins.Option(
			WithPort(10000, 11000),
			WithHost("0.0.0.0"),
			WithAuth(onAuth),
			WithPublic("/foo", GET, new(m_user.User), onFooPublic),
			WithPrivate("/foo", GET, new(m_user.User), onFooPrivate),
			WithUpload("/avatar", onUpload),
			WithUploadLimit(1024),
			WithHostHeader("X-Forwarded-Host"),
		)

		go func() {
			<-time.After(time.Hour)
			close <- true
		}()

		_ = comp.Start(close)
	})
}

type upload struct {
	id interface{}
}

func (u *upload) GetFileKey() string {
	return "avatar"
}

func (u *upload) GetHost() string {
	return "http://127.0.0.1:10000/"
}

func (u *upload) GetHostDir() string {
	return "."
}

func (u *upload) GetDir() string {
	return fmt.Sprintf("./%v/", u.id)
}

func (u *upload) GetFileName() string {
	return "avatar"
}

func onUpload(id interface{}) (UploadResponse, error) {
	return &upload{id: id}, nil
}

func onFooPublic(request Request, ip string) (Response, error) {
	fmt.Println(ip)
	req := request.(*m_user.User)
	req.Name = req.Email

	return req, nil
}

func onFooPrivate(id interface{}, request Request, ip string) (Response, error) {
	fmt.Println(ip)
	req := request.(*m_user.User)
	req.Lite = &m_user.UserLite{Id: util.ToInt64(id)}
	req.Name = req.Email

	return req, nil
}

func onAuth(request *http.Request) (interface{}, error) {
	return uid.Next(), nil
}

//func onFooPublic(request Request) (Response, int64, error) {
//	req := request.(*fooModel)
//	return &barModel{Hi: fmt.Sprintf("hi, %s", req.Name)}, 0, nil
//}
//
//func onFooPrivate(i interface{}, request Request) (Response, int64, error) {
//	req := request.(*fooModel)
//	return &barModel{Hi: fmt.Sprintf("hi, %s, your id: %v", req.Name, i)}, 0, nil
//}
//
//func onAuth(request *http.Request) (interface{}, error) {
//	//return nil, errors.New("reject")
//	return uid.Next(), nil
//}
//
//type upload struct {
//	Path string `json:"path"`
//}
//
//func onUpload(i interface{}) (Response, string, string, int64, error) {
//	path := fmt.Sprintf("./uploads/%v", 123)
//	return &upload{Path: path}, "avatar", path, 0, nil
//}
